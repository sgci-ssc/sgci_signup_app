#tacc/sgci_signup_app_web

FROM python:3.6

RUN mkdir /code

RUN pip install --upgrade pip && pip install uwsgi uwsgitop

RUN pip install https://projects.unbit.it/downloads/uwsgi-lts.tar.gz

COPY ./requirements.txt /code/requirements.txt
RUN pip install -r /code/requirements.txt

COPY . /code/
WORKDIR /code

CMD ["/usr/local/bin/uwsgi", "--ini", "sgci_signup_app/uwsgi.ini"]