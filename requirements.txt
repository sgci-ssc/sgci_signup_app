Django==2.0
djangorestframework==3.8.2
django-cors-headers==2.4.0
django-nocaptcha-recaptcha==0.0.20
-e git+https://github.com/TACC/agavepy.git@develop#egg=agavepy