from django.conf.urls import include, url
from django.contrib import admin
from rest_framework.urlpatterns import format_suffix_patterns

from .webapp import views as webapp_views

admin.autodiscover()

urlpatterns = [
    # create an account:
    url(r'^create_account', webapp_views.create_account,
        name='create_account'),

    # reset password:
    url(r'^reset_password_submit', webapp_views.reset_password_submit,
        name='reset_password_submit'),
    url(r'^reset_password', webapp_views.reset_password,
        name='reset_password'),


    # validate an account:
    url(r'^user/validate', webapp_views.user_validate,
        name='user_validate'),

    # rest API:
]

urlpatterns = format_suffix_patterns(urlpatterns)