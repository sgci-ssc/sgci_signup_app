__author__ = 'jstubbs'

from django.conf import settings
from django.template.context_processors import csrf
from django.core.mail import send_mail
from django.shortcuts import render
from django.views.decorators.http import require_GET, require_http_methods
from django.template.loader import get_template
from django.urls import reverse

import re
import json
import random
import urllib.request, urllib.parse, urllib.error
import urllib.request, urllib.error, urllib.parse

from agavepy.agave import Agave, AgaveException

import logging
logger = logging.getLogger(__name__)

ag = Agave(api_server=settings.AGAVE_TENANT_BASEURL,
           token=settings.AGAVE_TOKEN)

@require_http_methods(["GET", "POST"])
def create_account(request):
    """
    View to handle account creation in web app.
    """
    c = {}
    c.update(csrf(request))
    c['EMAIL_HOST'] = str(settings.EMAIL_HOST)
    c['EMAIL_PORT'] = str(settings.EMAIL_PORT)
    c['bottom_left_text'] = display_text_file('bottom_left_text.txt')
    c['bottom_right_text'] = display_text_file('bottom_right_text.txt')
    c['copyright'] = display_text_file('copyright.txt')

    if request.method == 'GET':
        return render(request, 'create_account.html', c)
    else :
        try:
            url = settings.NORECAPTCHA_SITE
            values = {
                'secret': settings.NORECAPTCHA_SECRET_KEY,
                'response': request.POST.get('g-recaptcha-response', None),
                'remoteip': request.META.get("REMOTE_ADDR", None),
            }

            data = urllib.parse.urlencode(values).encode('utf-8')
            req = urllib.request.Request(url, data)
            response = urllib.request.urlopen(req)
            result = json.loads(response.read())
            logger.debug('checking reCAPTCHA')

            attrs = request.POST

            username = attrs.get('username')
            first_name = attrs.get('first_name')
            last_name = attrs.get('last_name')
            full_name = attrs.get('full_name')
            email = attrs.get('email')
            password = attrs.get('password')
            
            if not result["success"]:
                logger.debug('reCAPTCHA failed')
                c = {
                    'username': username,
                    'email': email,
                    'first_name': first_name,
                    'last_name': last_name
                }
                c['error'] = 'I smell robots! Please prove your humanity by completing the reCAPTCHA :)'
                
            else:
                logger.debug('reCAPTCHA passed')
                c = {
                  'username' : username,
                  'email' : email,
                  'first_name' : first_name,
                  'last_name' : last_name
                }

                # Regex to check if username contains any special characters except underscores
                if re.search(r'\W', username): 
                        c['error'] = ("Error creating user; username contains special characters.")
                        return render(request, 'create_account.html', c)

                try:
                    if not username or len(username) == 0:
                        c['error'] = (
                            "Error creating user; username required.")
                        return render(request, 'create_account.html', c)
                    if not password or len(password) == 0:
                        c['error'] = (
                            "Error creating user; password required.")
                        return render(request, 'create_account.html', c)
                    if not email or len(email) == 0:
                        c['error'] = ("Error creating user; email required.")
                        return render(request, 'create_account.html', c)
                except Exception as e:
                    c['error'] = e
                    return render(request, 'create_account.html', c)

                if not request.POST.get("password2") == password:
                    logger.debug('Passwords do not match')
                    c['error'] = "Passwords do not match."
                    return render(request, 'create_account.html', c)

                try:
                    logger.debug('try Agavepy.profiles.create')
                    user = ag.profiles.create(body={'username':username, 'first_name':first_name,
                                       'last_name':last_name, 'email': email, 'password': password, 'status':'Inactive'})
                except Exception as e:
                    logger.debug('try Agavepy.profiles.create error ' + str(e))
                    c['error'] = 'Agave was unable to create a new user account. Please try again, or contact site administrator.'
                    return render(request, 'create_account.html', c)

                try:
                    send_mail(settings.NEW_ACCOUNT_EMAIL_SUBJECT, 
                              "Dear " + user.first_name + " " + user.last_name + ",\n" +
                              "Welcome to Science Gateways, we are looking forward to working with you!\n" +
                              "In order to activate your account click on the following link to confirm your email address:\n\n" +
                              settings.EMAIL_BASE_URL + reverse("user_validate") + "?username=" + user.username + "&nonce=" + user.nonce + "\n\n" +
                              "Thanks,\n" +
                              "Science Gateways Admin\n",
                              settings.NEW_ACCOUNT_FROM,
                              (user.email,))
                except Exception as e:
                    logger.debug('send_mail error ' + str(e))
                    c['error'] = 'Error sending activation email.'
                    return render(request, "create_account.html", c)

                c['account_created'] = True

        except Exception as e:
            logger.debug('create_account error ' + str(e))
            c['error'] = 'Account creation error. ' + str(e)

        return render(request, 'create_account.html', c)

@require_http_methods(["GET", "POST"])
def reset_password(request):
    c = {}
    c.update(csrf(request))
    c['bottom_left_text'] = display_text_file('bottom_left_text.txt')
    c['bottom_right_text'] = display_text_file('bottom_right_text.txt')
    c['copyright'] = display_text_file('copyright.txt')

    if request.method == 'GET':
        return render(request, 'reset_password.html', c)
    else:
        try:
            logger.debug('try Agavepy.profiles.listByUsername')
            username = request.POST.get('username')
            user = ag.profiles.listByUsername(username=username)
            if user:
                c['reset_password_sent'] = True
                send_mail(settings.RESET_ACCOUNT_EMAIL_SUBJECT,
                          "Dear " + user.first_name + " " + user.last_name + ",\n" +
                          "You requested to have your Science Gateways password reset.\n" +
                          "Please click on the link below to reset your password:\n\n" +  
                          settings.EMAIL_BASE_URL + reverse("reset_password_submit") + "?username=" + user.username + "&nonce=" + user.nonce + "\n\n" +
                          "Thanks,\n" +
                          "Science Gateways Admin\n",
                          settings.NEW_ACCOUNT_FROM,
                          (user.email, ))
        except Exception as e:
            logger.debug('Agavepy.profiles.listByUsername or send_mail error ' + str(e))
            c['error'] = 'Unable to locate user or send email.'

        return render(request, 'reset_password.html', c)


@require_http_methods(["GET", "POST"])
def reset_password_submit(request):
    """
    Reset an account password with the nonce that was sent in the email.
    """
    c = {}
    c.update(csrf(request))
    c['bottom_left_text'] = display_text_file('bottom_left_text.txt')
    c['bottom_right_text'] = display_text_file('bottom_right_text.txt')
    c['copyright'] = display_text_file('copyright.txt')

    if request.method == 'GET':
        username = request.GET.get('username')
        nonce = request.GET.get('nonce')
        
        try:
            logger.debug('try Agavepy.profiles.listByUsername ')
            user = ag.profiles.listByUsername(username=username)
        except Exception as e:
            logger.debug('Agavepy.profiles.listByUsername error' + str(e))
            c['error'] = "Error retrieving username."
            return render(request, 'reset_password_submit.html', c)

        if username and (nonce == user.nonce):
            c['username'] = username
            c['valid_request'] = True
        else:
            c['error'] = 'Your request could not be processed with the generated link. Please try re-setting your password again.'
            c['valid_request'] = False

    else:
        c['valid_request'] = True
        username = request.POST.get('username')
        password = request.POST.get('password')
        password2 = request.POST.get('password2')
        c['username'] = username

        try:
            logger.debug('try Agavepy.profiles.listByUsername ')
            user = ag.profiles.listByUsername(username=username)
        except Exception as e:
            logger.debug('Agavepy.profiles.listByUsername error' + str(e))
            c['error'] = "Agave had an error retrieving your username."
            return render(request, 'reset_password_submit.html', c)

        if not password == password2:
            logger.debug('Passwords do not match')
            c['error'] = "Passwords do not match."
            return render(request, 'reset_password_submit.html', c)

        try:
            user.password = password
            ag.profiles.update(username=user.username, body={'email':user.email, 'password':user.password})
            c['password_reset_success'] = True
        except Exception as e:
            logger.debug('Unable to reset account password ' + str(e))
            c['error'] = 'Agave was unable to reset account password.'
            render(request, 'reset_password_submit.html', c)

    return render(request, 'reset_password_submit.html', c)

@require_GET
def user_validate(request):
    """
    Validate an account with the nonce that was sent in the email.
    """
    logger.debug('User Validation beginning')
    c = {}
    c.update(csrf(request))
    c['bottom_left_text'] = display_text_file('bottom_left_text.txt')
    c['bottom_right_text'] = display_text_file('bottom_right_text.txt')
    c['copyright'] = display_text_file('copyright.txt')

    username = request.GET.get('username')
    nonce = request.GET.get('nonce')

    try:
        user = ag.profiles.listByUsername(username=username)
    except Exception as e:
        c['error'] = "Agave had an error retrieving your username."
        return render(request, 'activation.html', c)
    if user.nonce == nonce:
        try:  
            user.status  = 'Active'
            ag.profiles.update(username=username, body={'first_name':user.first_name, 'last_name':user.last_name, 'full_name':user.full_name, 'email': user.email, 'status': user.status, 'username':user.username})
        except Exception as e:
            logger.debug(str(e))
            render(request, 'activation.html', {
                   'error': 'Unable to activate account.'})
        return render(request, 'activation.html', {'account_activated': 'true', 'continue_url': settings.CONTINUE_URL})
        
    else:
        return render(request, 'activation.html', {'error': 'Invalid nonce'})


def display_text_file(filename):
    f = open(settings.BASE_DIR + '/webapp/static/text/' + filename, 'r')
    file_content = f.read()
    f.close()
    return file_content 
