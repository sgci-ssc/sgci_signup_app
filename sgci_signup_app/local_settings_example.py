"""
This is an example of the local_settings.py file and provides a minimal configuration to run the
service locally. Rename as local_settings.py and update as necessary for a production deployment.
"""

import os

# --------------------
# JWT Header settings
# -------------------
# Whether or not to check the JWT; When this is False, certain features will not be available such as the
# "me" lookup feature since these features rely on profile information in the JWT.
CHECK_JWT = os.environ.get('check_jwt', False)

# Actual header name that will show up in request.META; value depends on APIM configuration, in particular
# the tenant id specified in api-manager.xml
JWT_HEADER = os.environ.get('jwt_header', 'HTTP_JWT_ASSERTION')

# Relative location of the public key of the APIM instance; used for verifying the signature of the JWT.
#PUB_KEY = 'usersApp/agave-vdjserver-org_pub.txt'
PUB_KEY = '/home/apim/public_keys/apim_default.pub'

# APIM Role required to make updates to the LDAP database
USER_ADMIN_ROLE = 'Internal/user-account-manager'

# Whether or not the USER_ADMIN_ROLE before allowing updates to the LDAP db (/users service)
CHECK_USER_ADMIN_ROLE = True

# ------------------------------
# GENERAL SERVICE CONFIGURATION
# ------------------------------
# Base URL of this instance of the service. Used to populate the hyperlinks in the responses.
APP_BASE = 'http://localhost:8000'

# DEBUG = True turns up logging and causes Django to generate excpetion pages with stack traces and
# additional information. Should be False in production.
DEBUG = True

# With this setting activated, Django will not create test databases for any database which has
# the USE_LIVE_FOR_TESTS': True setting.
TEST_RUNNER = 'sgci_signup_app.testrunner.ByPassableDBDjangoTestSuiteRunner'

# ----------------------
# DATABASE CONNECTIVITY
# ----------------------
# for running in docker, need to get the ldap db connectivity data from the environment:
HERE = os.path.dirname(os.path.realpath(__file__))

DATABASES = {
    # Django requires a default db which we leave as the sql-lite default.
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(HERE, 'sgci_signup_app_sql'),
    }
}

# ----------------------
# WEB APP CONFIGURATION
# ----------------------


# These settings are only used when deploying the account sign up web application:
RESET_ACCOUNT_EMAIL_SUBJECT='Reset Science Gateway Account Password'
NEW_ACCOUNT_EMAIL_SUBJECT='New Science Gateway Account Requested'
NEW_ACCOUNT_FROM = 'do-not-reply@agaveapi.co'

# SMTP - used for the email loop account verification:
# Run the python smtp server with
# python -m smtpd -n -c DebuggingServer localhost:1025
# run an smtp server reachable by a docker container:
# python -m smtpd -n -c DebuggingServer 172.17.42.1:1025

mail_server = 'localhost'  # your mail server URL here
mail_server_port = 1025    # your mail server port  here
# print("Using: " + mail_server + " on port: ") + str(mail_server_port))
BASE_URL = '192.168.99.100'
BASE_PORT=8000
EMAIL_HOST = mail_server
EMAIL_PORT = mail_server_port
EMAIL_BASE_URL = 'http://' + BASE_URL + ':' + str(BASE_PORT)
CONTINUE_URL='https://catalog.sciencegateways.org' # your continue button url here
