# sgci_signup_app
This project works in conjuction with the agave deployer (https://bitbucket.org/agaveapi/deployer) workflow to allow SGCI tenant users to create accounts and reset passwords.

## Clone
```
$ git clone git@bitbucket.org:sgci-ssc/sgci_signup_app.git
$ cd sgci_signup_app
```
## Configure settings
Settings should be already set for your testing environemnt at sgci_signup_app/agave_id/local_settings_example.py:
```
BASE_URL='192.168.99.100'
BASE_PORT=8000
EMAIL_HOST = mail_server            # localhost
EMAIL_PORT = mail_server_port       # 1025
EMAIL_BASE_URL = 'http://' + BASE_URL + ':' + str(BASE_PORT)
CONTINUE_URL='https://catalog.sciencegateways.org'   # your continue button url here
```
## Build
For a local-only build:
```
$ docker-compose -f docker-compose.yml build
$ docker-compose -f docker-compose.yml up
```

For a deployment build:
```
$ docker-compose -f docker-compose-deploy.yml build
$ docker-compose -f docker-compose-deploy.yml up
```

If the `docker-compose-deploy.yml build` is successful, you should see the `sgci_signup_app_nginx_1` and `sgci_signup_app_web_1` containers:
```
$ docker ps
COMMAND                   STATUS   PORTS                                             NAMES
"nginx -g 'daemon of…"    Up       0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp          sgci_signup_app_nginx_1
"/usr/local/bin/uwsg…"    Up       0.0.0.0:1025->1025/tcp, 0.0.0.0:8000->8000/tcp    sgci_signup_app_web_1
```

## Test
To test the email functionality of the webapp, you'll need to run the commands below.
```
$ docker exec -it sgci_signup_app_web_1 bash
>>> python -m smtpd -n -c DebuggingServer localhost:1025
```
### Creating an account
You should be able to test the webapp now in your browser by going to: `localhost:80/create_account`. All of the fields are required.  Submitting this form will make an `create_account` API call to Agave.  After submitting the form, you should see something like this on your debugging server:
```
---------- MESSAGE FOLLOWS ----------
MIME-Version: 1.0
Content-Type: text/plain; charset="utf-8"
Content-Transfer-Encoding: 7bit
Subject: New Agave Account Requested
From: do-not-reply@agaveapi.co
To: mrojas@tacc.utexas.edu
Date: Fri, 03 Feb 2017 15:25:36 -0000
Message-ID: <20170203152536.8.62220@afbb44ecb783>
X-Peer: 127.0.0.1

Dear Manuel Rojas,
Welcome to Science Gateways, we are looking forward to working with you!
In order to activate your account click on the following link to confirm your email address:

http://192.168.99.100:8000/user/validate?username=mrojas&nonce=381758974

Thanks,
Science Gateways Admin
------------ END MESSAGE ------------
```
Finish the account registration by going to the URL in the email.  Submit the form again to be taken to the Science Gateways Catalogue.

### Resetting a Password
To test resetting a password go to `http://192.168.99.100:8000/reset_password`. After submitting the form, you should see something like this on your debugging server: 
```
---------- MESSAGE FOLLOWS ----------
MIME-Version: 1.0
Content-Type: text/plain; charset="utf-8"
Content-Transfer-Encoding: 7bit
Subject: New Agave Account Requested
From: do-not-reply@agaveapi.co
To: mrojas@tacc.utexas.edu
Date: Fri, 03 Feb 2017 15:25:36 -0000
Message-ID: <20170203152536.8.62220@afbb44ecb783>
X-Peer: 127.0.0.1

Dear Manuel Rojas,
You requested to have your Science Gateways password reset.
Please click on the link below to reset your password:

http://192.168.99.100/reset_password_submit?username=mrojas&nonce=660881597

Thanks,
Science Gateways Admin
------------ END MESSAGE ------------
```
Finish the password reset by going to the URL in the email.  Submit the form to reset your password.

## Production
Build and update sgci_signup_app_web image:
```
$ docker build --no-cache -t tacc/sgci_signup_app_web .
$ docker push tacc/sgci_signup_app_web:latest
```

Build and update sgci_signup_app_nginx image:
```
$ docker build -t tacc/sgci_signup_nginx -f nginx/Dockerfile .
$ docker push tacc/sgci_signup_nginx:latest
```